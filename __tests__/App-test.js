import 'react-native';
import React from 'react';
import App from '../src/App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

const EnzimeConfiguration =  Enzyme.configure({ adapter: new Adapter()});
//import EnzimeConfiguration from '../jest/setup';


it('renders correctly', ()=> {
  renderer.create(<App />);
});


describe('render App', ()=> {
  it('should match to snapshot', ()=>{
    const snap = renderer.create(
      <App/>
    ).toJSON();
    expect(snap).toMatchSnapshot()
  });

  it('placeNameChangeHandler should setState with {name: "a firstname"}',()=>{
    const wrapper = shallow(<App/>);
    const app = wrapper.instance();
    app.placeNameChangeHandler('Matt');
    expect(app.state.placeName).toEqual({name: 'Matt'})
  })
});






