# basic configuration

{
  "name": "redline",
  "version": "0.0.1",
  "private": true,
  "scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "test": "jest",
    "ios": "react-native run-ios",
    "android": "react-native run-android",
    "reset": "react-native start --reset-cache"
  },
  "dependencies": {
    "core-js": "^3.0.1",
    "react": "16.8.3",
    "react-native": "0.59.8"
  },
  "devDependencies": {
    "@babel/core": "^7.4.4",
    "@babel/preset-env": "^7.4.4",
    "@babel/preset-react": "^7.0.0",
    "@babel/runtime": "^7.4.4",
    "babel-cli": "^6.26.0",
    "babel-eslint": "^10.0.1",
    "babel-jest": "^24.7.1",
    "babel-preset-flow": "^6.23.0",
    "enzyme": "^3.9.0",
    "eslint": "^5.5.0",
    "eslint-config-airbnb": "^17.1.0",
    "eslint-plugin-flowtype": "^3.7.0",
    "eslint-plugin-import": "^2.17.2",
    "eslint-plugin-jsx-a11y": "^6.2.1",
    "eslint-plugin-react": "^7.13.0",
    "flow-bin": "^0.93.0",
    "flow-typed": "^2.5.1",
    "jest": "^24.7.1",
    "enzyme-adapter-react-16": "^1.13.0",
    "metro-react-native-babel-preset": "^0.54.0",
    "react-dom": "^16.8.6",
    "react-test-renderer": "16.8.3"
  },
  "jest": {
    "preset": "react-native"
  }
}