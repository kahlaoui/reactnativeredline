/**
 * @format
 * @flow
 */

const keyMapGenerator = (): string => {
    return Math.floor(Date.now()/ Math.random()).toString();
}


export default keyMapGenerator;