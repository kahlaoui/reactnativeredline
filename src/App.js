/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import keyMapGenerator from "./utils/keyMapGenerator";
//components
import ListItem from "./dumbComponents/ListItem/ListItem.js";
import PlaceInput from "./dumbComponents/PlaceInput/PlaceInput.js";
import PlaceList from "./dumbComponents/PlaceList/PlaceList.js";
import PlaceDetail from "./dumbComponents/PlaceDetail/PlaceDetail";
//redux
import { connect } from "react-redux";
import { addPlace_, deletePlace_, selectPlace_, deselectPlace_} from "./store/actions/index";
//assets
//import placeImage from "./asset/space_tech.png";

type appComponentState = {
  placeName: string,
};

type AppComponentProps = {
  onAddPlace: function,
  onSelectPlace: function,
  onDeletePlace: function,
  onDeselectPlace: function,
  placeName: {| name: string |},
  placeList: Array<{| name: string, placeKey: string, image: {| uri: string |} |}>,
  placeKey: string,
  selectedPlace: null | {|
    name: string,
    placeKey: string,
    image: {| uri: string |}
    |}
};


class App extends Component<AppComponentProps, appComponentState> {
 
  state = {
    placeName: ""
  };
 
  placeNameChangeHandler = (text: string) => {
    this.setState({ placeName: text });
  };

  placeSubmitHandler = () => {
    this.props.onAddPlace(this.state.placeName);
    this.setState(state => ({placeName : ""}));
  };

  placeSelectedHandler = ( placeKey: string)=> {
    this.props.onSelectPlace(placeKey);
  };
  
  placeDeletedHandler = () => {
    if(this.props.selectedPlace !== null){
      this.props.onDeletePlace(this.props.selectedPlace.placeKey);
    }
  };

  modalClosedHandler = () => {
    this.props.onDeselectPlace();
  };

  render() {
    return (
      <View style={styles.container}>
        <PlaceDetail
          selectedPlace={this.props.selectedPlace}
          onItemDeleted={this.placeDeletedHandler}
          onModalClosed={this.modalClosedHandler}
        />
        <PlaceInput
          name={this.state.placeName}
          placeNameChangeHandler={this.placeNameChangeHandler}
          placeSubmitHandler={this.placeSubmitHandler}
        />
        <PlaceList
          placeList={this.props.placeList}
          onItemSelected={this.placeSelectedHandler}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 26,
    backgroundColor: "#fff"
  }
});

const mapStateToProps = state => {
  return {
    placeName: state.places.placeName, 
    placeList: state.places.placeList,
    placeKey:  state.places.placeKey,
    selectedPlace: state.places.selectedPlace
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAddPlace: (name) => dispatch(addPlace_(name)),
    onDeletePlace: (placeKey) => dispatch(deletePlace_(placeKey)),
    onSelectPlace: (placeKey) => dispatch(selectPlace_(placeKey)),
    onDeselectPlace: () => dispatch(deselectPlace_())
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(App);