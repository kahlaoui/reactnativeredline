/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStore, combineReducers, compose} from "redux";
import placesReducer from "./reducers/places_Reducer";


const rootReducer = combineReducers({
  places: placesReducer
});

let composeEnhancers = compose;

if(__DEV__){
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = (): function => {
  return createStore(rootReducer, composeEnhancers());
};

export default configureStore;