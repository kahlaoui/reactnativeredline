/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {ADD_PLACE, DELETE_PLACE, SELECT_PLACE, DESELECT_PLACE} from "../actions/types";
import keyMapGenerator from "../../utils/keyMapGenerator";

const initialState = {
  placeName: {payload: { name: "" }} ,
  placeList: [],
  placeKey: "",
  selectedPlace: null
};

type Action = {
  type: string,
  payload: any 
}


type State = {
  placeName: {payload: {| name: string |}},
  placeList: Array<{| name: string, placeKey: string, image: {| uri: string |} |}>,
  placeKey: string, 
  selectedPlace: null | 
    {|name: string,
      placeKey: string,
      image: {| uri: string |}|}
};

const placesReducer = (state: State = initialState, action: Action): Object => {
  switch (action.type){
  case ADD_PLACE:
    return {
      ...state,
      placeList : state.placeList.concat({
        placeKey: keyMapGenerator(),
        name: action.payload,
        image: {
          uri:
              "https://androidportal.zoznam.sk/wp-content/uploads/2014/05/surface-planets.jpg"
        }
      })
    };
  case DELETE_PLACE:
    return {
      placeList : state.placeList.filter(
        (place) => place.placeKey !== action.payload
      ),
      selectedPlace: null
    };
  case SELECT_PLACE:
    return {
      ...state,
      selectedPlace: state.placeList.find(place => {
        return place.placeKey === action.payload;
      })
    };
  case DESELECT_PLACE:
    return {
      ...state,
      selectedPlace: null
    };
  default:
    return state;
  }
};

export default placesReducer;