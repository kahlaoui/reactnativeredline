/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {ADD_PLACE, DELETE_PLACE, SELECT_PLACE, DESELECT_PLACE} from "./types"; 

export const addPlace_ = (placeName: string) => {
  return {
    type: ADD_PLACE,
    payload: placeName
  };
};

export const deletePlace_ = (placeKey: string) => {
  return {
    type: DELETE_PLACE,
    payload: placeKey
  };
};

export const selectPlace_ = (placeKey: string) => {
  return {
    type: SELECT_PLACE,
    payload: placeKey
  };
};

export const deselectPlace_ = () =>{
  return {
    type: DESELECT_PLACE
  };
};