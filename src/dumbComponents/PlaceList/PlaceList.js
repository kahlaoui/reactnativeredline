
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
 
import React from "react";
import { FlatList, View, StyleSheet } from "react-native";
import ListItem from "../ListItem/ListItem.js"; 


type ListProps = {
    placeList: Array<{| name: string, placeKey: string, image: {| uri: string|}|}>,
    onItemSelected: function
}

const PlaceList = (props: ListProps) => {
  
  return (
    <View style={styles.listContainer}>
      <FlatList
        data={props.placeList}
        renderItem={({item})=>{
          return <ListItem placeImg={item.image} nameItem={item.name} onPressed={()=>props.onItemSelected(item.placeKey)}/>;
        }}
      />
    </View>  
  );
};

const styles = StyleSheet.create({ 
  listContainer: {
    margin: 5,
    width: "100%"
  }
});


export default PlaceList;