/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
 
import React from "react";
import { View, TextInput, Button, StyleSheet } from "react-native";


type PlaceInputProps = {
    placeNameChangeHandler: function,
    placeSubmitHandler: function,
    name: string
};

const PlaceInput = (props: PlaceInputProps) => {
  return ( 
    <View style={styles.inputContainer}>
      <TextInput 
        style={styles.Input}
        placeholder='awesome place'
        onChangeText={(eventText)=>props.placeNameChangeHandler(eventText)} 
        value={props.name}/>
      <Button onPress={props.placeSubmitHandler} styles={styles.placeButton} title='Add'/>
    </View>
  );
};

const styles = StyleSheet.create({
  Input: {
    width:"70%",
    borderColor:"black",
    borderWidth: 2
  },
  inputContainer: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  }
})
 
export default PlaceInput;