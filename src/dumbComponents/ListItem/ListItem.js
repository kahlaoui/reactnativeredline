import React from 'react';
import { ios } from '../../utils/constant_env/constant_env.js';
import { 
     View,
     Text, 
     FlatList, 
     StyleSheet, 
     TouchableOpacity, 
     TouchableNativeFeedback, 
     Platform,
     Image} from 'react-native';

type ListItemProps = {
    nameItem: string,
    onPressed: function,
    placeImg: any
}

const ListItem = (props: ListItemProps) => {
    return ( 
        <View>
            {Platform.OS === ios? 
            <TouchableOpacity onPress={props.onPressed}> 
                <View style={styles.listItem}>
                    <Image resizeMode="cover" source={props.placeImg} style={styles.placeImage}/>
                    <Text >{props.nameItem}</Text>
                </View>
            </TouchableOpacity>
            :
            <TouchableNativeFeedback onPress={props.onPressed}> 
                <View  style={styles.listItem}>
                    <Image source={props.placeImg} style={styles.placeImage}/>
                    <Text >{props.nameItem}</Text>
                </View>
            </TouchableNativeFeedback>
            } 
        </View>
     )
}

const styles = StyleSheet.create({
    listItem: {
        padding : 10,
        marginBottom: 5 ,
        borderColor : 'pink',
        borderWidth: 1,
        backgroundColor: '#eee',
        flexDirection: 'row',
        alignItems: 'center'
    },
    placeImage: {
        margin: 8,
        width: 100,
        height: 100
    }
})
 
export default ListItem;