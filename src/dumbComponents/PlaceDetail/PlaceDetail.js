/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
 
import React from "react";
import { Modal, View, Image, Button, Text, StyleSheet } from "react-native";


type PlaceDetailProps = {
    selectedPlace: null | any,
    onItemDeleted: function,
    onModalClosed: function
}


const PlaceDetail = (props: PlaceDetailProps) => {
  let modalContent = null;
  const key = props.selectedPlace ? props.selectedPlace.key : null;
  if(props.selectedPlace){
    modalContent = (
      <View>
        <Image source={props.selectedPlace.image} style={styles.placeImage}/>
        <Text style={styles.placeName} >{props.selectedPlace.name}</Text>
      </View>
    );
  }
  return ( 
    <Modal 
      onRequestClose={props.onModalClosed}
      visible={props.selectedPlace !== null} 
      animationType="slide">
      <View style={styles.modalContainer}>
        {modalContent}
        <View>
          <Button title={"Delete"} color="red" onPress={()=>props.onItemDeleted(key)}/>
          <Button title={"Close"} onPress={props.onModalClosed}/>
        </View>
      </View>
    </Modal>
  );
};
 
const styles = StyleSheet.create({
  modalContainer: {
    margin: 22
  },
  placeImage: {
    marginTop:30,
    width: "100%",
    height: 200
  },
  placeName : {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 28
  }
});

export default PlaceDetail;