// flow-typed signature: a35158bffe6e7ee7bad13b0dda55689a
// flow-typed version: <<STUB>>/babel-jest_v^24.7.1/flow_v0.93.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'babel-jest'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'babel-jest' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'babel-jest/build/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'babel-jest/build/index.js' {
  declare module.exports: $Exports<'babel-jest/build/index'>;
}
